function Pokemon(name,level){
	this.name = name;
	this.level = level;
	this.health = level*100;
	this.attack = 10*level;
	this.tackle =function(target){
		console.log(this.name+" used tackle.")
		target.health -= this.attack;
		console.log(target.name+" took "+this.attack+" damage.")
		if(target.health<=0){
			faint(target);
		}
	}
}
let trainer ={
	name:"Ash Ketchum",
	age: 20,
	pokemon:["Pikachu",'Pichu','Raichu'],
	friends:[{name:"Brock",age:20},{name:"misty",age:20}],
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
}

function faint(target){
	console.log(`${target.name} has fainted`);
}
console.log(`My name is ${trainer.name} and I am ${trainer.age} years old.
I am a Pokemon trainer and my Pokemon deck includes: ${trainer.pokemon.join(", ")}.
	My friends are: 
	${trainer.friends.map(({name,age})=>name+": "+age+" years old").join(", ")}.`);
trainer.talk();

let pikachu = new Pokemon("Pikachu",10);
let pichu = new Pokemon("Pichu",1);
console.log(pikachu.tackle(pichu));
